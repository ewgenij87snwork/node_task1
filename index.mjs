import * as myUtils from 'test-utils';
import util from 'util';

const params = { password: 'sdf234fdsf32cdsc' };

const promiseFunc = util.promisify(myUtils.runMePlease);

try {
    const result = await promiseFunc(params);
} catch (error) {
    console.log('error', error);
}
